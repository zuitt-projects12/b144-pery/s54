let collection = [];

// Write the queue functions below.
const print = () => collection;

const enqueue = (element) => {
    if (collection.length === 0){
        collection[0] = element;
    }
    else{
        collection[collection.length] = element;
    }
    return collection;
};

const dequeue = () => {
    let newArr = [];
    for(let i = 0; i < collection.length - 1; i++){
        if(i===0){
            for(let j=i; j<collection.length-1; j++){
                collection[j] = collection[j+1];
            }
        }
        for(let i = 0; i < collection.length - 1; i++){
            newArr[i] = collection[i];
        }
    }
    collection.length = 0;
    for(i = 0; i < newArr.length; i++){
        collection[i] = newArr[i];
    }
    return collection;
};

const front = () => collection[0];

const size = () => collection.length;

const isEmpty = (collection) => collection === [];

module.exports = {
    collection,
    print,
    enqueue,
    dequeue,
    front,
    size,
    isEmpty
};